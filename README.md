# README #

#### WARNING ####
**The application is not totally implemented yet!!!**

### What is this repository for? ###

WS Project:    
This is the server-side application that runs on server exposing a set of services as required for the project.
The services are:

* *getAllSimpleProducts*
* *getAllFullProducts*
* *singleSelectSimpleProductById/{Id}*
* *singleSelectFullProductById/{Id}*
* *getChildProductsById/{Id}*
* *getImagesByProductsId/{Id}*

Client:    
Client application to integrate with the server.


### How do I get set up? ###

You must have a server and deploy the project through your IDE.
The dependency management is set by maven.
When you first run the project you may want to run the ProductJPA.java as application since it will persist a few objects in the Database.
When the program creates the Database i'll create a stucture called "banco" in your "C:\" location that will persist the data.
To run the services, deploy them to a server.
To test the serverside you may want to import the SoapRestSever.xml into a Soap-ui or similar to check the services.    
To run the client, just go to host:port/Client/index.jsp. You can search the products from there.    


### Who do I talk to? ###

Thales Gomes
thalesgomes93@gmail.com

### FAQ ###

"How to compile and run the application with an example for each call."
You should run the client and the server side application, once you are done you may check the {host:port}/Client/index.jsp.

"How to run the suite of automated tests."
This feature were not implemented until the moment.

"Mention anything that was asked but not delivered and why, and any additional comments."
I did no finished the job with the desired performance, but i feel like i learned a lot from this "POC". I don't usually use all of this together to build a client/server with 3 days at all.
I had to learn a lot of new ways of get work done (hibernate, maven dependencies, etc)
That sayid, i want to thank all of you for the attention and the interest in me as candidate for your tests, I had lot of fun building this project from scratch.