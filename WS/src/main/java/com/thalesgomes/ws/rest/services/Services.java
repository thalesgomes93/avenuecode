package com.thalesgomes.ws.rest.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thalesgomes.ws.rest.classes.Image;
import com.thalesgomes.ws.rest.classes.Product;

/*a) Get all products excluding relationships (child products, images)
b) Get all products including specified relationships (child product and/or images)
c) Same as 1 using specific product identity
d) Same as 2 using specific product identity
e) Get set of child products for specific product
f) Get set of images for specific product*/

@Path("/WebServices")
public class Services {

	public static EntityManager getManager() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Persistencia");
		return factory.createEntityManager();
	}

	@GET
	@Path("/getAllSimpleProducts")
	@Produces({ MediaType.APPLICATION_JSON })
	public String getAllSimpleProducts() {
		EntityManager em = getManager();
		em.getTransaction().begin();
		List<Product> listProduct = null;
		try {
			listProduct = em.createQuery("FROM Product", Product.class).getResultList();

			if (!listProduct.isEmpty()) {
				for (Product product : listProduct) {
					product.setChildProducts(null);
					product.setImages(null);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			em.close();
		}
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create(); // Validar
																										// anotations
																										// @Exposed nas
																										// classes
		String json = gson.toJson(listProduct);

		return json;
	}

	@GET
	@Path("/getAllFullProducts")
	@Produces({ MediaType.APPLICATION_JSON })
	public String getAllFullProducts() {
		EntityManager em = getManager();
		em.getTransaction().begin();
		List<Product> listProduct = null;
		try {
			listProduct = em.createQuery("FROM Product", Product.class).getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			em.close();
		}
		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create(); // Validar
																										// anotations
																										// @Exposed nas
																										// classes
		String json = gson.toJson(listProduct);

		return json;
	}

	@GET
	@Path("/singleSelectSimpleProductById/{Id}") // /WS/services/WebServices/singleSelectSimpleProductById/0
	@Produces({ MediaType.APPLICATION_JSON })
	public String singleSelectSimpleProductById(@PathParam("Id") Integer id) {
		EntityManager em = getManager();
		em.getTransaction().begin();

		Product product = null;
		String json = null;
		try {
			product = em.find(Product.class, id);
			if(product!=null) {
				product.setChildProducts(null);
				product.setImages(null);
			}
			Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create(); 
			json = gson.toJson(product);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			em.close();
		}
		return json;
	}

	@GET
	@Path("/singleSelectFullProductById/{Id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public String singleSelectFullProductById(@PathParam("Id") Integer id) {
		EntityManager em = getManager();
		em.getTransaction().begin();

		Product product = null;
		String json = null;
		try {
			product = em.find(Product.class, id);
			Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create(); // Validar
																											// anotations
																											// @Exposed
																											// nas
																											// classes
			json = gson.toJson(product);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			em.close();
		}
		return json;
	}

	@GET
	@Path("/getChildProductsById/{Id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public String getChildProductsById(@PathParam("Id") Integer id) {
		EntityManager em = getManager();
		em.getTransaction().begin();
		String json = null;
		List<Product> p = em.createQuery("SELECT p FROM Product p WHERE p.id LIKE ?1").setParameter(1, id)
				.getResultList();
		List<Product> children = null;

		if (!p.isEmpty()) {
			for (Product product : p) {
				children = product.getChildProducts();
			}
		}

		try {
			Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create(); // Validar
																											// anotations
																											// @Exposed
																											// nas
																											// classes
			json = gson.toJson(children);
		} catch (Exception e) {
			// TODO: handle exception
			em.close();
			e.printStackTrace();
		} finally {
			em.close();
		}
		return json;
	}

	@GET
	@Path("/getImagesByProductsId/{Id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public String getImagesByProductsId(@PathParam("Id") Integer id) {
		EntityManager em = getManager();
		em.getTransaction().begin();
		String json = null;
		List<Product> p = em.createQuery("SELECT p FROM Product p WHERE p.id LIKE ?1").setParameter(1, id)
				.getResultList();
		List<Image> images = null;

		if (!p.isEmpty()) {
			for (Product product : p) {
				images = product.getImages();
			}
		}

		try {
			Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
			json = gson.toJson(images);
		} catch (Exception e) {
			// TODO: handle exception
			em.close();
			e.printStackTrace();
		} finally {
			em.close();
		}
		return json;
	}

}
