package com.thalesgomes.ws.rest.classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name = "PRODUCT")
public class Product implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PRODUCT_ID")
	@Expose
	private Integer id;

	@ManyToOne(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(name = "PARENT_ID")
	private Product parent;

	@Column(name = "NAME")
	@Expose
	private String name;

	@Column(name = "DESCRIPTION")
	@Expose
	private String description;

	@Expose
	@OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
	private List<Product> childProducts = new ArrayList<Product>();
	
	@Expose
	@OneToMany(mappedBy = "product", fetch = FetchType.LAZY)
	private List<Image> images = new ArrayList<Image>();

	// Getters and Setters
	public Product getParent() {
		return parent;
	}

	public void setParent(Product parent) {
		this.parent = parent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Product> getChildProducts() {
		return childProducts;
	}

	public void setChildProducts(List<Product> childProducts) {
		this.childProducts = childProducts;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

}