package com.thalesgomes.ws.rest.JPA;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.thalesgomes.ws.rest.classes.Image;
import com.thalesgomes.ws.rest.classes.Product;

public class ProductJPA {

	public static EntityManager getManager() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("Persistencia");
		return factory.createEntityManager();
	}

	public static void main(String[] args) {
		populateDatabase();
		System.exit(0);
	}

	public static String getProducts() {
		EntityManager em = getManager();
		em.getTransaction().begin();
		List<Product> products = em.createQuery("from Product", Product.class).getResultList();
		String json = null;

		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();

		try {
			json = gson.toJson(products);
		} catch (Exception e) {
			// TODO: handle exception
			em.close();
			e.printStackTrace();
		} finally {
			em.close();
		}
		return json;

	}

	public static String getSingleProduct() {
		EntityManager em = getManager();
		em.getTransaction().begin();
		String json = null;
		Integer param = 6;
		List<Product> p = em.createQuery("SELECT p FROM Product p WHERE p.id LIKE ?1").setParameter(1, param)
				.getResultList();

		try {
			Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();
			json = gson.toJson(p.get(0));
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Produto n�o encontrado!");
			em.close();
			e.printStackTrace();
		} finally {
			em.close();
		}
		return json;

	}

	public static void populateDatabase() {
		// Cria Produto Pai
		Product parent = new Product();
		parent.setDescription("descriptionParent");
		parent.setName("nameParent");
		parent.setParent(null);

		// Cria Produto Filho
		Product child1 = new Product();
		child1.setName("name1");
		child1.setDescription("description1");
		child1.setParent(parent);
		parent.getChildProducts().add(child1);
		
		// Cria Produto Filho
		Product child2 = new Product();
		child2.setName("name2");
		child2.setDescription("description2");
		child2.setParent(parent);
		parent.getChildProducts().add(child2);

		Image image = new Image();
		image.setType("jpeg");
		image.setProduct(parent);
		parent.getImages().add(image);

		EntityManager em = getManager();
		em.getTransaction().begin();
		em.persist(child1);
		em.persist(child2);
		em.persist(parent);
		em.persist(image);
		em.getTransaction().commit();
		em.close();

		System.out.println("Commit done.");

	}

}
