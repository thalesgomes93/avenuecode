var myApp = angular.module('ClientRest', []);

myApp
		.controller(
				"MainController",
				function($scope, $http) {
					$scope.myList = [ {
						name : "0",
						value : "Busca de Produtos Simples",
						show : false
					}, {
						name : "1",
						value : "Busca de Produtos Detalhada",
						show : false
					}, {
						name : "2",
						value : "Busca Simples de um Produto por ID",
						show : true
					}, {
						name : "3",
						value : "Busca Detalhada de um Produto por ID",
						show : true
					}, {
						name : "4",
						value : "Busca de Produtos relacionados a um Produto",
						show : true
					}, {
						name : "5",
						value : "Busca de imagens para um Produto Espec�fico",
						show : true
					} ];


					$scope.request = function() {						
						
						/*a) Get all products excluding relationships (child products, images)
						b) Get all products including specified relationships (child product and/or images)
						c) Same as 1 using specific product identity
						d) Same as 2 using specific product identity
						e) Get set of child products for specific product
						f) Get set of images for specific product*/
						
						switch ($scope.selectedItem.name) {
						case "0":
							$http.get('http://localhost:8080/WS/services/WebServices/getAllSimpleProducts').then(function(response) {
								$scope.Products = response.data;
								$scope.auxMessage="1- Buscar todos os produtos, excluindo os relacionamentos (Produtos Filhos, Imagens)";
							})
							break;
						case "1":
							$http.get('http://localhost:8080/WS/services/WebServices/getAllFullProducts').then(function(response) {
								$scope.Products = response.data;
								$scope.auxMessage="2- Buscar todos os produtos, incluindo relacionamentos espec�ficos (Produtos Filhos e/ou Imagens)";
							})
							break;
						case "2":
							$http.get('http://localhost:8080/WS/services/WebServices/singleSelectSimpleProductById/'+$scope.stringFind).then(function(response) {
								$scope.Products = response.data;
								$scope.auxMessage="1- Buscar todos os produtos por ID, excluindo os relacionamentos (Produtos Filhos, Imagens)";
							})
							break;
						case "3":
							$http.get('http://localhost:8080/WS/services/WebServices/singleSelectFullProductById/'+$scope.stringFind).then(function(response) {
								$scope.Products = response.data;
								$scope.auxMessage="4- Buscar todos os produtos por ID, incluindo relacionamentos espec�ficos (Produtos Filhos e/ou Imagens)";
							})
							break;
						case "4":
							$http.get('http://localhost:8080/WS/services/WebServices/getChildProductsById/'+$scope.stringFind).then(function(response) {
								$scope.Products = response.data;
								$scope.auxMessage="5- Buscar um conjunto de Produtos Filhos para um Producto espec�fico";
							})
							break;
						case "5":
							$http.get('http://localhost:8080/WS/services/WebServices/getImagesByProductsId/'+$scope.stringFind).then(function(response) {
								$scope.Products = response.data;
								$scope.auxMessage="5- Buscar um conjunto de Imagens para um Producto espec�fico";
							})
							break;
						default:
							$scope.auxMessage ="Op��o inv�lida"
						break;
						}
						
						
						
						
						
					}

				});
