<!doctype html>
<html ng-app="ClientRest">
<head>
<title>Client Rest</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular.min.js"></script>
<script src="mainController.js"></script>
<script type="text/javascript"
	src="webjars/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>

<body>
	<h1>Lista de Produtos</h1>
	<div ng-controller="MainController">
		<div>
			<p>
				<b><strong>{{auxMessage}}</strong></b>
			</p>
		</div>

		<select ng-options="option.value for option in myList"
			ng-model="selectedItem">
		</select> <input ng-model="stringFind" ng-trim="true" ng-show=selectedItem.show></input>
		<div>
			<button ng-click="request()">Buscar</button>
		</div>
		<div class="container">
			<h1>Produtos:</h1>
			<table class="table" border="1">
				<thead>
					<tr>
						<th>ID DO PRODUTO</th>
						<th>NOME DO PRODUTO</th>
						<th>DESCRI��O DO PRODUTO</th>
						<th>PRODUTOS FILHOS</th>
						<th>IMAGENS</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="product in Products">
						<td>{{product.id}}</td>
						<td>{{product.name}}</td>
						<td>{{product.description}}</td>
						<td>{{product.childProducts}}</td>
						<td>{{product.images}}</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div>Retorno do Servi�o: {{Products}}</div>
	</div>
</body>
</html>